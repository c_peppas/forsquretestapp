Android OS Support
==================
API 19+ (Landscape support)
 
Architecture & Main Libs/frameworks
===================================
MVP (Model View Presenter)

Dagger2

RxJava2

Retrofit2

Fresco

AppCompat

ConstrainLayout

Mockito

Espresso

RestMock(wrapper of OkHttp's MockWebServer)
     
Unit Tests
==========

covering the following classes

ExploreVenuesPresenter
RemoteVenuesDataSource
VenuesRepositoryImpl
ErrorMessageHelperImpl

```groovy
./gradlew testDebugUnitTest
```

Tests reports found app/build/reports/tests/testDebugUnitTest/index.html

UI Espresso integration Tests (via Spoon Runner)
================================================

covering basic integration tests on 
ExploreVenuesActivity

Showcases how I use espresso by following the Robot Pattern (or PageObject Martin Fowler).

Also showcases how DI injection helps us via TestComponent and overriden DaggerModules 
to mock APIs by injecting dependencies that gives the right test environment.

For Api Mocking I used OkHttp's MockWebServer wrapped by RestMock (more fluent API)
- https://github.com/square/okhttp/tree/master/mockwebserver
- https://github.com/andrzejchm/RESTMock

Test reports and screens via Spoon https://github.com/square/spoon

```groovy
./gradlew spoonDebugAndroidTest
```

Find generated UI spoon reports with screenshots @ app/build/spoon/debug/index.html


Caching Strategy Notes
=======================

Http Disk cache is enabled from http request if the server supports max-age header.

In-Memory Short Cache is used via InMemoryVenuesDataSource in the VenuesRepositoryImpl to hold data for 30s in order to prevent
excessive http calls in a short period of time. 

Useful for device rotation use cases or autocomplete searches etc.

VenuesRepositoryImpl is using 2 data sources (InMemory and Remote) and via concat and takefirst operators is managing the caching/loading mechanism. 

This mechanism is explained in detail http://blog.danlew.net/2015/06/22/loading-data-from-multiple-sources-with-rxjava/

Forsquare API AUTH Token Note
========================
The auth token which is used in the app is taken from the forsquare public API explorer
and it might expire without notice. 
https://developer.foursquare.com/docs/explore#req=venues/explore%3Fll%3D40.7,-74
