package com.example.forsquareapp.component;

import com.example.forsquareapp.di.component.ApplicationComponent;
import com.example.forsquareapp.di.module.AndroidModule;
import com.example.forsquareapp.di.module.DataModule;
import com.example.forsquareapp.di.module.NetworkModule;
import com.example.forsquareapp.di.module.RxModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AndroidModule.class, RxModule.class, NetworkModule.class, DataModule.class})
public interface TestApplicationComponent extends ApplicationComponent {

}
