package com.example.forsquareapp.explore;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.forsquareapp.TestApplication;
import com.example.forsquareapp.explore.ui.ExploreVenuesActivity;
import com.example.forsquareapp.utils.DeviceAnimationTestRule;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.appflate.restmock.RESTMockServer;
import okhttp3.mockwebserver.MockResponse;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static io.appflate.restmock.utils.RequestMatchers.pathContains;

/**
 * Example for basic tests to showcase Integration UI tests with RobotPattern(aka PageObject).
 */
@RunWith(AndroidJUnit4.class)
public class ExploreVenuesActivityTest {

    private ExploreVenuesScreenRobot mRobot = new ExploreVenuesScreenRobot();
    private TestApplication mApplication;

    @Rule
    public ActivityTestRule<ExploreVenuesActivity> mActivityRule = new ActivityTestRule<>(ExploreVenuesActivity.class, true, false);

    @ClassRule
    static public DeviceAnimationTestRule deviceAnimationTestRule = new DeviceAnimationTestRule();

    @Before
    public void setUp() throws Exception {
        mApplication = (TestApplication) getInstrumentation().getTargetContext().getApplicationContext();
        RESTMockServer.reset();
    }

    @Test
    public void shouldDisplaySearchResults() throws Exception {
        //GIVEN MockWebServer WILL accept the following API Requests and Return mock Responses
        RESTMockServer.whenGET(pathContains("explore"))
                .thenReturnFile(200, "response.venues/explore_success_200_full.json");

        mActivityRule.launchActivity(ExploreVenuesActivity.createIntent(mApplication));

        mRobot.takeScreenShot(mActivityRule.getActivity(), "initial_state")
                .searchFor("Athens")
                .takeScreenShot(mActivityRule.getActivity(), "search_results")
                .checkIfSpecificVenueIsDisplayed("Guarantee");
    }

    @Test
    public void shouldDisplayNoResultsFound() throws Exception {
        //GIVEN
        RESTMockServer.whenGET(pathContains("explore"))
                .thenReturnFile(400, "response.venues/explore_error_400_full.json");

        mActivityRule.launchActivity(ExploreVenuesActivity.createIntent(mApplication));

        String seachTerm = "Athensdsdss";

        mRobot.takeScreenShot(mActivityRule.getActivity(), "initial_state")
                .searchFor(seachTerm)
                .takeScreenShot(mActivityRule.getActivity(), "no_results")
                .checkIfNoResultsTextIsDisplayed(mApplication, seachTerm);
    }

    @Test
    public void shouldDisplayErrorMessageOnHttpError() throws Exception {
        //GIVEN
        RESTMockServer.whenGET(pathContains("explore"))
                .thenReturn(new MockResponse().setResponseCode(401));

        mActivityRule.launchActivity(ExploreVenuesActivity.createIntent(mApplication));

        String seachTerm = "Athensdsdss";

        mRobot.takeScreenShot(mActivityRule.getActivity(), "initial_state")
                .searchFor(seachTerm)
                .takeScreenShot(mActivityRule.getActivity(), "no_results")
                .checkIfNoResultsTextIsDisplayed(mActivityRule.getActivity(), seachTerm)
                .checkIfErrorToastIsDisplayed(mActivityRule.getActivity(), 401);
    }
}
