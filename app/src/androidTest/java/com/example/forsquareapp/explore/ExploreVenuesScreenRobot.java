package com.example.forsquareapp.explore;

import android.app.Activity;
import android.content.Context;

import com.example.forsquareapp.R;
import com.example.forsquareapp.utils.Robot;


public class ExploreVenuesScreenRobot extends Robot<ExploreVenuesScreenRobot> {

    public ExploreVenuesScreenRobot() {

    }

    public ExploreVenuesScreenRobot searchFor(String searchTerm) {
        enterTextInSeachView(searchTerm);
        return this;
    }

    public ExploreVenuesScreenRobot checkIfSpecificVenueIsDisplayed(String place) {
        checkViewWithTextIfDisplayed(place);
        return this;
    }

    public ExploreVenuesScreenRobot checkIfNoResultsTextIsDisplayed(Context context, String searchTerm) {
        checkViewWithTextIfDisplayed(context.getString(R.string.no_results_for, searchTerm));
        return this;
    }

    public ExploreVenuesScreenRobot checkIfErrorToastIsDisplayed(Activity activity, int errorCode) {
        checkIfToastIsDisplayed(activity, activity.getString(R.string.error_http_code, errorCode));
        return this;
    }
}
