package com.example.forsquareapp;

import com.example.forsquareapp.base.BaseApplication;
import com.example.forsquareapp.component.DaggerTestApplicationComponent;
import com.example.forsquareapp.component.TestApplicationComponent;
import com.example.forsquareapp.di.module.AndroidModule;
import com.example.forsquareapp.di.module.NetworkModule;
import com.example.forsquareapp.di.module.RxModule;
import com.example.forsquareapp.network.EnvironmentConfig;
import com.example.forsquareapp.utils.schedulers.EspressoSchedulers;
import com.example.forsquareapp.utils.schedulers.RxSchedulers;

import io.appflate.restmock.RESTMockServer;
import okhttp3.HttpUrl;

/**
 *
 */

public class TestApplication extends BaseApplication {
    @Override
    protected TestApplicationComponent createApplicationComponent() {
        return DaggerTestApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .networkModule(new NetworkModule() {
                    @Override
                    public HttpUrl provideHttpUrl(EnvironmentConfig config) {
                        return HttpUrl.parse(RESTMockServer.getUrl());
                    }
                })
                .rxModule(new RxModule() {
                    @Override
                    public RxSchedulers provideSchedulers() {
                        return new EspressoSchedulers();
                    }
                })
                .build();
    }
}
