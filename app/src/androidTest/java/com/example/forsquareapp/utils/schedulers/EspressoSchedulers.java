package com.example.forsquareapp.utils.schedulers;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class EspressoSchedulers implements RxSchedulers {
    @NonNull
    @Override
    public Scheduler computation() {
        return Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @NonNull
    @Override
    public Scheduler io() {
        return Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @NonNull
    @Override
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}
