package com.example.forsquareapp.utils;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.test.espresso.contrib.RecyclerViewActions;

import com.jraska.falcon.FalconSpoon;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;

public abstract class Robot<T extends Robot> {

    protected T checkTextIsDisplayed(String text, @IdRes int view) {
        onView(allOf(withId(view), withText(text))).check(matches(isDisplayed()));
        return (T) this;
    }

    protected T checkTextIsDisplayed(@StringRes int resId, @IdRes int view) {
        onView(allOf(withId(view), withText(resId))).check(matches(isDisplayed()));
        return (T) this;
    }

    protected T clickViewWithText(String text) {
        onView(withText(text)).check(matches(isDisplayed())).perform(click());
        return (T) this;
    }

    protected T checkViewWithId(@IdRes int viewId) {
        onView(withId(viewId)).check(matches(isDisplayed()));
        return (T) this;
    }

    protected T checkViewWithTextIfDisplayed(String text) {
        onView(withText(text)).check(matches(isDisplayed()));
        return (T) this;
    }

    protected T selectPositionInRecyclerView(int position, @IdRes int recyclerView) {
        onView(withId(recyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(position, click()));
        return (T) this;
    }

    protected T checkIfToastIsDisplayed(Activity activity, String text) {
        onView(withText(text)).inRoot(withDecorView(not(is(activity
                .getWindow().getDecorView())))).check(matches(isDisplayed()));
        return (T) this;
    }

    protected T checkIfDialogIsDisplayed(@StringRes int text) {
        onView(withText(text)).check(matches(isDisplayed()));
        return (T) this;
    }

    protected T enterText(@IdRes int id, String text) {
        onView(withId(id)).perform(typeText(text));
        return (T) this;
    }

    protected T enterTextInSeachView(String text) {
        onView(withId(android.support.design.R.id.search_src_text)).perform(typeText(text));
        return (T) this;
    }

    public T takeScreenShot(Activity activity, String tag) {
        // todo comment out this when your run test via Android Studio.
        FalconSpoon.screenshot(activity, tag);
        return (T) this;
    }

    public T openMenuDrawer() {
        //onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        return (T) this;
    }

    public <K> K toRobot(Class<K> robotClass) throws IllegalAccessException, InstantiationException {
        return robotClass.newInstance();
    }

    // Ugly method that help us overcome the problem on Handler.postDelayed messages that espresso does not handle.
    protected void blockMainThreadFor(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}