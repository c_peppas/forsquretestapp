package com.example.forsquareapp.explore;

import android.support.annotation.StringRes;

import com.example.forsquareapp.R;
import com.example.forsquareapp.explore.ui.ExploreVenuesView;
import com.example.forsquareapp.model.entities.Venue;
import com.example.forsquareapp.model.repo.VenuesRepository;
import com.example.forsquareapp.utils.FakeExceptionUtils;
import com.example.forsquareapp.utils.error.ErrorMessageHelperImpl;
import com.example.forsquareapp.utils.error.MessageBundle;
import com.example.forsquareapp.utils.schedulers.TestRxSchedulers;
import com.google.gson.JsonParseException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ExploreVenuesPresenterTest {

    private ExploreVenuesPresenter presenter;

    @Mock ExploreVenuesView view;
    @Mock VenuesRepository repository;

    @Before
    public void setUp() throws Exception {
        presenter = new ExploreVenuesPresenter(view, repository,
                new ErrorMessageHelperImpl(),
                new TestRxSchedulers(),
                new CompositeDisposable());
    }

    @Test(expected = NullPointerException.class)
    public void shouldExitWithExceptionWhenSearchTermIsNull() throws Exception {
        presenter.search(null);
    }

    @Test
    public void shouldResetViewStateWhenSearchTermIsLessThan4Characters() throws Exception {
        presenter.search("123");

        verify(view, times(1)).clearList();
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).hideNoResultsFound();
        verify(view, times(1)).cancelExistingError();

        verifyNoMoreInteractions(view);
    }

    @Test
    public void shouldLoadVenuesItemsSuccessfully() throws Exception {
        ArrayList<Venue> venues = new ArrayList<Venue>() {{
            add(new Venue.Builder().id("001").build());
            add(new Venue.Builder().id("002").build());
            add(new Venue.Builder().id("003").build());
        }};

        when(repository.getRecommendedVenuesByPlace("Athens")).thenReturn(Single.fromCallable(() -> venues));

        presenter.search("Athens");

        verify(view, times(1)).showProgress();
        verify(view, times(1)).hideNoResultsFound();
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).showVenues(venues);

        verifyNoMoreInteractions(view);
    }

    @Test
    public void shouldShowNoResultsFoundForEmptyList() throws Exception {

        when(repository.getRecommendedVenuesByPlace("asdas")).thenReturn(Single.fromCallable(ArrayList::new));

        presenter.search("asdas");

        verify(view, times(1)).showProgress();
        verify(view, times(1)).hideNoResultsFound();
        verify(view, times(1)).hideProgress();
        verify(view, times(1)).showNoResultsFound("asdas");
        verify(view, times(1)).clearList();

        verifyNoMoreInteractions(view);
    }

    @Test
    public void shouldShowJsonParsingErrorToast() throws Exception {

        when(repository.getRecommendedVenuesByPlace("asdas")).thenReturn(Single.error(new JsonParseException("")));

        presenter.search("asdas");

        verifyViewInteractionWithErrorStringRes("asdas", R.string.error_parsing_error, null);
    }

    @Test
    public void shouldShowUnexpectedErrorToast() throws Exception {

        when(repository.getRecommendedVenuesByPlace("asdas")).thenReturn(Single.error(new NoSuchElementException("")));

        presenter.search("asdas");

        verifyViewInteractionWithErrorStringRes("asdas", R.string.error_unexpected, null);
    }

    @Test
    public void shouldShowHttpError() throws Exception {

        when(repository.getRecommendedVenuesByPlace("asdas")).thenReturn(Single.error(FakeExceptionUtils.createHttpException(403)));

        presenter.search("asdas");

        verifyViewInteractionWithErrorStringRes("asdas", R.string.error_http_code, 403);
    }

    @Test
    public void shouldShowNoConnectionError() throws Exception {

        when(repository.getRecommendedVenuesByPlace("asdas")).thenReturn(Single.error(new UnknownHostException()));

        presenter.search("asdas");

        verifyViewInteractionWithErrorStringRes("asdas", R.string.error_no_connection, null);
    }

    @Test
    public void shouldShowTimeoutError() throws Exception {

        when(repository.getRecommendedVenuesByPlace("asdas")).thenReturn(Single.error(new SocketTimeoutException()));

        presenter.search("asdas");

        verifyViewInteractionWithErrorStringRes("asdas", R.string.error_time_out, null);
    }

    private void verifyViewInteractionWithErrorStringRes(String searchTerm, @StringRes int id, Object... args) {
        verify(view, times(1)).showProgress();
        verify(view, times(1)).hideNoResultsFound();

        verify(view, times(1)).cancelExistingError();

        if (args != null) {
            verify(view, times(1)).showError(new MessageBundle(id, args));
        } else {
            verify(view, times(1)).showError(new MessageBundle(id));
        }

        verify(view, times(1)).hideProgress();
        verify(view, times(1)).showNoResultsFound(searchTerm);
        verify(view, times(1)).clearList();

        verifyNoMoreInteractions(view);
    }
}