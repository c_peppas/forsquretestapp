package com.example.forsquareapp.utils.error;

import com.example.forsquareapp.R;
import com.example.forsquareapp.utils.FakeExceptionUtils;
import com.google.gson.JsonParseException;

import org.junit.Before;
import org.junit.Test;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class ErrorMessageHelperImplTest {

    Exception exception;

    MessageHelper messageHelper;

    @Before
    public void setUp() throws Exception {
        messageHelper = new ErrorMessageHelperImpl();
    }

    @Test
    public void shouldReturnUnexpectedMessage() throws Exception {
        exception = new Exception("");

        assertThat(messageHelper.errorMessage(exception).id(), is(R.string.error_unexpected));
    }

    @Test
    public void shouldReturnHttpErrorMessage() throws Exception {
        exception = FakeExceptionUtils.createHttpException(404);

        assertThat(messageHelper.errorMessage(exception).id(), is(R.string.error_http_code));
    }

    @Test
    public void shouldReturnNoConnectionMessage() throws Exception {
        exception = new UnknownHostException();

        assertThat(messageHelper.errorMessage(exception).id(), is(R.string.error_no_connection));
    }

    @Test
    public void shouldReturnRequestTimeOutMessage() throws Exception {
        exception = new SocketTimeoutException();

        assertThat(messageHelper.errorMessage(exception).id(), is(R.string.error_time_out));
    }

    @Test
    public void shouldReturnJsonParsingMessage() throws Exception {
        exception = new JsonParseException("");

        assertThat(messageHelper.errorMessage(exception).id(), is(R.string.error_parsing_error));
    }
}