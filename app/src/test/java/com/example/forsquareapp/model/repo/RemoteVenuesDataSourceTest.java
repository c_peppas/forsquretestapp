package com.example.forsquareapp.model.repo;

import com.example.forsquareapp.model.entities.Venue;
import com.example.forsquareapp.model.entities.Venues;
import com.example.forsquareapp.network.ForsquareApi;
import com.example.forsquareapp.utils.FakeResponseHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.UnknownHostException;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import retrofit2.HttpException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RemoteVenuesDataSourceTest {

    VenuesDataSource remoteSource;

    Gson gson;
    FakeResponseHelper<Venues> fakeResponseHelper;
    TestObserver<Data<List<Venue>>> testObserver;

    @Mock ForsquareApi api;

    @Before
    public void setUp() throws Exception {
        gson = new GsonBuilder().setLenient().create();
        remoteSource = new RemoteVenuesDataSource(api);
        fakeResponseHelper = new FakeResponseHelper<>(gson);
        testObserver = new TestObserver<>();
    }

    @Test
    public void shouldReturn3VenuesSuccess() throws Exception {

        String searchTerm = "Athens";

        when(api.exploreVenues(ForsquareApi.AUTH_TOKEN,
                ForsquareApi.VERSION,
                searchTerm,
                ForsquareApi.ALLOW_PHOTOS,
                ForsquareApi.ITEM_LIMIT))
                .thenReturn(fakeResponseHelper.successResponse(Venues.class, "response.venues/explore_success_200.json"));

        remoteSource.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        testObserver.assertComplete();
        testObserver.assertNoErrors();

        //find a better way to lookup items from testObserver
        Data<List<Venue>> items = (Data<List<Venue>>) testObserver.getEvents().get(0).get(0);

        assertThat(items.getItems().size(), is(3));
    }

    @Test
    public void shouldReturnZeroVenues() throws Exception {
        // we found that the following search term returns empty array of venues.
        String searchTerm = "asdas";

        when(api.exploreVenues(ForsquareApi.AUTH_TOKEN,
                ForsquareApi.VERSION,
                searchTerm,
                ForsquareApi.ALLOW_PHOTOS,
                ForsquareApi.ITEM_LIMIT))
                .thenReturn(fakeResponseHelper.successResponse(Venues.class, "response.venues/explore_success_empty_200.json"));

        remoteSource.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        testObserver.assertComplete();
        testObserver.assertNoErrors();

        Data<List<Venue>> venues = (Data<List<Venue>>) testObserver.getEvents().get(0).get(0);

        assertThat(venues.getItems().size(), is(0));
    }

    @Test
    public void shouldHandleMalformedResponse() throws Exception {
        String searchTerm = "none";

        when(api.exploreVenues(ForsquareApi.AUTH_TOKEN,
                ForsquareApi.VERSION,
                searchTerm,
                ForsquareApi.ALLOW_PHOTOS,
                ForsquareApi.ITEM_LIMIT))
                .thenReturn(fakeResponseHelper.successResponse(Venues.class, "response.venues/explore_success_200_malformed.json"));

        remoteSource.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        testObserver.assertError(throwable -> throwable instanceof JsonParseException);
    }

    @Test
    public void shouldHandleHttpErrorResponse() throws Exception {
        String searchTerm = "none";

        when(api.exploreVenues(ForsquareApi.AUTH_TOKEN,
                ForsquareApi.VERSION,
                searchTerm,
                ForsquareApi.ALLOW_PHOTOS,
                ForsquareApi.ITEM_LIMIT))
                .thenReturn(fakeResponseHelper.errorResponse(400, ""));

        remoteSource.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        testObserver.assertError(throwable -> throwable instanceof HttpException);
    }

    @Test
    public void shouldHandleClientErrorResponse() throws Exception {
        String searchTerm = "none";

        when(api.exploreVenues(ForsquareApi.AUTH_TOKEN,
                ForsquareApi.VERSION,
                searchTerm,
                ForsquareApi.ALLOW_PHOTOS,
                ForsquareApi.ITEM_LIMIT))
                .thenReturn(Observable.error(new UnknownHostException()));

        remoteSource.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        testObserver.assertError(throwable -> throwable instanceof UnknownHostException);
    }
}