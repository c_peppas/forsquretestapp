package com.example.forsquareapp.model.repo;


import com.example.forsquareapp.model.entities.Venue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class VenuesRepositoryImplTest {

    private VenuesRepositoryImpl repository;

    @Mock InMemoryVenuesDataSource cacheSource;
    @Mock RemoteVenuesDataSource remoteSource;
    @Mock Data<List<Venue>> cachedData;
    @Mock Data<List<Venue>> remoteData;

    ArrayList<Venue> cachedVenues;
    ArrayList<Venue> remoteVenues;

    TestObserver testObserver;

    @Before
    public void setUp() throws Exception {
        repository = new VenuesRepositoryImpl(cacheSource, remoteSource);

        testObserver = new TestObserver();

        cachedVenues = new ArrayList<Venue>() {{
            add(new Venue.Builder().id("001").build());
            add(new Venue.Builder().id("002").build());
            add(new Venue.Builder().id("003").build());
        }};

        remoteVenues = new ArrayList<Venue>() {{
            add(new Venue.Builder().id("001").build());
            add(new Venue.Builder().id("002").build());
        }};

        when(cachedData.getItems()).thenReturn(cachedVenues);
        when(remoteData.getItems()).thenReturn(remoteVenues);
    }

    @Test
    public void shouldReturnInMemoryDataWhenCacheHasNotExpired() throws Exception {
        String searchTerm = "test";

        when(cacheSource.getRecommendedVenuesByPlace(searchTerm)).thenReturn(Observable.fromCallable(() -> cachedData));
        when(remoteSource.getRecommendedVenuesByPlace(searchTerm)).thenReturn(Observable.fromCallable(() -> remoteData));

        when(cachedData.isUpToDate()).thenReturn(true);
        when(remoteData.isUpToDate()).thenReturn(true);

        repository.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        testObserver.assertComplete();
        testObserver.assertNoErrors();

        testObserver.assertValue(cachedVenues);
    }

    @Test
    public void shouldReturnRemoteDataWhenCacheHasExpired() throws Exception {
        String searchTerm = "test";

        when(cacheSource.getRecommendedVenuesByPlace(searchTerm)).thenReturn(Observable.fromCallable(() -> cachedData));
        when(remoteSource.getRecommendedVenuesByPlace(searchTerm)).thenReturn(Observable.fromCallable(() -> remoteData));

        when(cachedData.isUpToDate()).thenReturn(false);
        when(remoteData.isUpToDate()).thenReturn(true);

        repository.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        testObserver.assertComplete();
        testObserver.assertNoErrors();

        testObserver.assertValue(remoteVenues);
    }

    @Test
    public void shouldReturnRemoteDataWhenCacheIsEmpty() throws Exception {
        String searchTerm = "test";

        when(cacheSource.getRecommendedVenuesByPlace(searchTerm)).thenReturn(Observable.fromCallable(() -> null));
        when(remoteSource.getRecommendedVenuesByPlace(searchTerm)).thenReturn(Observable.fromCallable(() -> remoteData));

        when(remoteData.isUpToDate()).thenReturn(true);

        repository.getRecommendedVenuesByPlace(searchTerm).subscribe(testObserver);

        verify(cacheSource, times(1)).saveVenues(searchTerm, remoteVenues);

        testObserver.assertComplete();
        testObserver.assertNoErrors();

        testObserver.assertValue(remoteVenues);
    }
}