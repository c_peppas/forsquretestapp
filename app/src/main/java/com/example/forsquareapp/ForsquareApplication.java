package com.example.forsquareapp;

import com.example.forsquareapp.base.BaseApplication;
import com.example.forsquareapp.di.component.ApplicationComponent;
import com.example.forsquareapp.di.component.DaggerApplicationComponent;
import com.example.forsquareapp.di.module.AndroidModule;

/**
 *
 */
public class ForsquareApplication extends BaseApplication {

    @Override
    protected ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }
}
