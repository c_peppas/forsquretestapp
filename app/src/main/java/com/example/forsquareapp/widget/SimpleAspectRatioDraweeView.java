package com.example.forsquareapp.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * An version of {@link SimpleDraweeView}  that calculates the new height based respecting
 * a given aspect ratio.
 */
public class SimpleAspectRatioDraweeView extends SimpleDraweeView {

    public SimpleAspectRatioDraweeView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
    }

    public SimpleAspectRatioDraweeView(Context context) {
        super(context);
    }

    public SimpleAspectRatioDraweeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SimpleAspectRatioDraweeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public SimpleAspectRatioDraweeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //todo write comment about aspect ration and pass it as attr in xml
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = (int) (width / 0.7837f);
        setMeasuredDimension(width, height);
    }
}
