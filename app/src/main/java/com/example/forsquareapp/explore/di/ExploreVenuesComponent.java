package com.example.forsquareapp.explore.di;

import com.example.forsquareapp.di.scope.ActivityScope;
import com.example.forsquareapp.explore.ui.ExploreVenuesActivity;

import dagger.Subcomponent;

/**
 *
 */
@ActivityScope
@Subcomponent(modules = {ExploreVenuesModule.class})
public interface ExploreVenuesComponent {
    void inject(ExploreVenuesActivity target);
}
