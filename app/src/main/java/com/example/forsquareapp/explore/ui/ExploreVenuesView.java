package com.example.forsquareapp.explore.ui;

import com.example.forsquareapp.model.entities.Venue;
import com.example.forsquareapp.utils.error.MessageBundle;

import java.util.List;

/**
 *
 */
public interface ExploreVenuesView {
    void showProgress();
    void hideProgress();
    void showVenues(List<Venue> venues);
    void showNoResultsFound(String place);
    void hideNoResultsFound();

    void showError(MessageBundle bundle);
    void cancelExistingError();

    void clearList();
}
