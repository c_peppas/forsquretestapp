package com.example.forsquareapp.explore;


import com.example.forsquareapp.explore.ui.ExploreVenuesView;
import com.example.forsquareapp.model.repo.VenuesRepository;
import com.example.forsquareapp.utils.error.MessageHelper;
import com.example.forsquareapp.utils.Strings;
import com.example.forsquareapp.utils.schedulers.RxSchedulers;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

import static com.example.forsquareapp.utils.Preconditions.checkNotNull;

/**
 *
 */
public class ExploreVenuesPresenter {

    private final ExploreVenuesView view;
    private final VenuesRepository repository;
    private final RxSchedulers schedulers;
    private final CompositeDisposable disposables;
    private final MessageHelper messageHelper;

    @Inject
    public ExploreVenuesPresenter(ExploreVenuesView view, VenuesRepository repository,
                                  MessageHelper messageManager,
                                  RxSchedulers schedulers, CompositeDisposable disposables) {
        this.view = view;
        this.repository = repository;
        this.schedulers = schedulers;
        this.disposables = disposables;
        this.messageHelper = messageManager;
    }

    public void search(String place) {
        checkNotNull(place, "You must supply a place search query");

        disposables.clear();

        if (Strings.isBlank(place) || place.trim().length() <= 3) {
            resetViewState();
            return;
        }

        Disposable disposable =
                repository.getRecommendedVenuesByPlace(place)
                        .subscribeOn(schedulers.io())
                        .observeOn(schedulers.ui())
                        .doOnSubscribe(ignore -> {
                            view.showProgress();
                            view.hideNoResultsFound();
                        })
                        .onErrorReturn(throwable -> {
                            if (!resultsNotFound(throwable)) {
                                view.cancelExistingError();
                                view.showError(messageHelper.errorMessage(throwable));
                            }
                            return Collections.emptyList();
                        })
                        .subscribe(venues -> {
                            view.hideProgress();
                            if (venues.isEmpty()) {
                                view.showNoResultsFound(place);
                                view.clearList();
                            }else {
                                view.showVenues(venues);
                            }
                        }, throwable -> {
                        });

        disposables.add(disposable);
    }

    private boolean resultsNotFound(Throwable throwable) {
        return throwable instanceof HttpException &&
                ((HttpException) throwable).code() == 400;
    }

    public void unSubscribe() {
        disposables.clear();
    }

    private void resetViewState() {
        view.clearList();
        view.hideProgress();
        view.hideNoResultsFound();
        view.cancelExistingError();
    }
}