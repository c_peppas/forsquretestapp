package com.example.forsquareapp.explore.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.forsquareapp.R;
import com.example.forsquareapp.base.BaseActivity;
import com.example.forsquareapp.explore.ExploreVenuesPresenter;
import com.example.forsquareapp.explore.di.ExploreVenuesModule;
import com.example.forsquareapp.model.entities.Venue;
import com.example.forsquareapp.utils.OnQueryTextListenerAdapter;
import com.example.forsquareapp.utils.error.MessageBundle;
import com.example.forsquareapp.utils.ui.MarginDecoration;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExploreVenuesActivity extends BaseActivity implements ExploreVenuesView{
    private static final String BUNDLE_RECYCLER_VIEW_STATE = "BUNDLE_RECYCLER_VIEW_STATE";

    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.search_view) SearchView searchView;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.no_results_for) TextView noResultsForView;

    @Inject ExploreVenuesPresenter presenter;

    private Toast errorToast;
    private ExploreVenuesAdapter adapter;
    private Parcelable recyclerViewState;

    public static Intent createIntent(Context context) {
        return new Intent(context, ExploreVenuesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appComponent().add(new ExploreVenuesModule(this)).inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_explore_venues);

        ButterKnife.bind(this);

        setUpSearchView();
        setUpRecyclerView();

        restoreSavedStateValues(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BUNDLE_RECYCLER_VIEW_STATE, recyclerView.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        presenter.unSubscribe();
        cancelExistingError();
    }

    @Override public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override public void showVenues(List<Venue> venues) {
        adapter.updateItems(venues);
        delegateRestoreRecyclerViewStateToLayoutManager();
    }

    @Override public void showNoResultsFound(String place) {
        noResultsForView.setText(getString(R.string.no_results_for, place));
        noResultsForView.setVisibility(View.VISIBLE);
    }

    @Override public void hideNoResultsFound() {
        noResultsForView.setVisibility(View.GONE);
    }

    @Override public void showError(MessageBundle message) {
        errorToast = Toast.makeText(this, message.getString(this), Toast.LENGTH_LONG);
        errorToast.show();
    }

    @Override public void cancelExistingError() {
        if (errorToast != null) {
            errorToast.cancel();
        }
    }

    @Override public void clearList() {
        adapter.updateItems(Collections.emptyList());
    }

    private void setUpSearchView() {
        searchView.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        searchView.setOnQueryTextListener(new OnQueryTextListenerAdapter() {
            @Override public boolean onQueryTextChange(String newText) {
                presenter.search(newText);
                return super.onQueryTextChange(newText);
            }
        });
    }

    private void setUpRecyclerView() {
        recyclerView.addItemDecoration(new MarginDecoration(this, R.dimen.space_medium));
        recyclerView.setHasFixedSize(true);
        adapter = new ExploreVenuesAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                hideKeyboard();
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void delegateRestoreRecyclerViewStateToLayoutManager() {
        if (recyclerViewState != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            recyclerViewState = null;
        }
    }

    private void restoreSavedStateValues(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            recyclerViewState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_VIEW_STATE);
        }
    }
}
