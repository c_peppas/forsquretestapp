package com.example.forsquareapp.explore.di;

import com.example.forsquareapp.di.scope.ActivityScope;
import com.example.forsquareapp.explore.ExploreVenuesPresenter;
import com.example.forsquareapp.explore.ui.ExploreVenuesView;
import com.example.forsquareapp.model.repo.VenuesRepository;
import com.example.forsquareapp.utils.error.MessageHelper;
import com.example.forsquareapp.utils.error.ErrorMessageHelperImpl;
import com.example.forsquareapp.utils.schedulers.RxSchedulers;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 *
 */
@Module
public class ExploreVenuesModule {
    private final ExploreVenuesView mView;

    public ExploreVenuesModule(ExploreVenuesView view) {
        this.mView = view;
    }

    @Provides @ActivityScope
    public MessageHelper provideErrorManager() {
        return new ErrorMessageHelperImpl();
    }

    @Provides @ActivityScope
    public ExploreVenuesPresenter providesPresenter(VenuesRepository repository, MessageHelper manager,
                                                    RxSchedulers schedulers, CompositeDisposable disposable) {
        return new ExploreVenuesPresenter(mView, repository, manager, schedulers, disposable);
    }
}
