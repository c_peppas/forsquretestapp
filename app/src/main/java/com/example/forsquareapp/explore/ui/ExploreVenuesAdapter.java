package com.example.forsquareapp.explore.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.forsquareapp.R;
import com.example.forsquareapp.model.entities.Venue;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 */
public class ExploreVenuesAdapter extends RecyclerView.Adapter<ExploreVenuesAdapter.VenueViewHolder> {

    private List<Venue> items = Collections.emptyList();

    @Override public VenueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_venue_item, parent, false);
        VenueViewHolder viewHolder = new VenueViewHolder(view);
        return viewHolder;
    }

    @Override public void onBindViewHolder(VenueViewHolder holder, int position) {
        holder.bindView(items.get(position));
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public void updateItems(List<Venue> items) {
        this.items = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    static class VenueViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.venue_name) TextView name;
        @BindView(R.id.venue_category) TextView category;
        @BindView(R.id.venue_image) SimpleDraweeView imageView;
        @BindView(R.id.venue_category_icon) SimpleDraweeView icon;

        public VenueViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(Venue item) {
            name.setText(item.name);
            category.setText(item.firstCategoryName());
            imageView.setImageURI(item.firstFeaturePhotoUrl());
            icon.setImageURI(item.firstCategoryIconUrl());
        }
    }
}