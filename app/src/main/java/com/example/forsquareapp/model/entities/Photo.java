package com.example.forsquareapp.model.entities;

/**
 *
 */

public class Photo {
    public String id;
    public String prefix;
    public String suffix;
    public int width;
    public int height;
}
