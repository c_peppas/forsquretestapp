package com.example.forsquareapp.model.entities;

/**
 *
 */

public class Icon {
    public static final String ICON_SIZE = "32";

    public String prefix;
    public String suffix;
}
