package com.example.forsquareapp.model.repo;

/**
 *
 */
public class Data<T> {

    private static final long STALE_MS = 30 * 1000; // Data is stale after 30 seconds

    private final T items;
    private final long timestamp;

    public Data(T items) {
        this.items = items;
        this.timestamp = System.currentTimeMillis();
    }

    public boolean isUpToDate() {
        return System.currentTimeMillis() - timestamp < STALE_MS;
    }

    public T getItems() {
        return items;
    }
}
