package com.example.forsquareapp.model.repo;


import com.example.forsquareapp.model.entities.Venue;

import java.util.List;

import io.reactivex.Observable;

public interface VenuesDataSource {

    Observable<Data<List<Venue>>> getRecommendedVenuesByPlace(String place);

    void saveVenues(String place, List<Venue> venues);
}
