package com.example.forsquareapp.model.entities;

import java.util.List;
import java.util.Objects;

/**
 *
 */
public class Venue {
    public final String id;
    public final String name;
    public final List<VenueCategory> categories;
    public final FeaturedPhotos featuredPhotos;

    private Venue(Builder builder) {
        id = builder.id;
        name = builder.name;
        categories = builder.categories;
        featuredPhotos = builder.featuredPhotos;
    }

    public static final class Builder {
        private String id;
        private String name;
        private List<VenueCategory> categories;
        private FeaturedPhotos featuredPhotos;

        public Builder() {
        }

        public Builder(Venue copy) {
            this.id = copy.id;
            this.name = copy.name;
            this.categories = copy.categories;
            this.featuredPhotos = copy.featuredPhotos;
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder categories(List<VenueCategory> val) {
            categories = val;
            return this;
        }

        public Builder featuredPhotos(FeaturedPhotos val) {
            featuredPhotos = val;
            return this;
        }

        public Venue build() {
            return new Venue(this);
        }
    }

    public String firstFeaturePhotoUrl() {
        if (featuredPhotos != null
                && featuredPhotos.items != null
                && !featuredPhotos.items.isEmpty()
                && featuredPhotos.items.get(0).prefix != null
                && featuredPhotos.items.get(0).suffix != null
                && featuredPhotos.items.get(0).width != 0
                && featuredPhotos.items.get(0).height != 0) {
            Photo firstPhoto = featuredPhotos.items.get(0);
            return firstPhoto.prefix + firstPhoto.width + "x" + firstPhoto.height + firstPhoto.suffix;
        }
        return "N/A";
    }

    public String firstCategoryIconUrl() {
        if (categories != null && !categories.isEmpty()
                && categories.get(0).icon != null
                && categories.get(0).icon.prefix != null
                && categories.get(0).icon.suffix != null) {
            Icon firstCategoryIcon = categories.get(0).icon;
            return firstCategoryIcon.prefix + Icon.ICON_SIZE + firstCategoryIcon.suffix;
        }
        return "N/A";
    }

    public String firstCategoryName() {
        if (categories != null && !categories.isEmpty()
                && categories.get(0).name != null) {
            return categories.get(0).name;
        }
        return "N/A";
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venue venue = (Venue) o;
        return Objects.equals(id, venue.id);
    }

    @Override public int hashCode() {
        return Objects.hash(id);
    }
}
