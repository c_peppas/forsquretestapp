package com.example.forsquareapp.model.repo;


import com.example.forsquareapp.model.entities.Venue;

import java.util.List;

import io.reactivex.Single;

public interface VenuesRepository {

    Single<List<Venue>> getRecommendedVenuesByPlace(String place);

    void saveVenues(String place, List<Venue> venues);
}
