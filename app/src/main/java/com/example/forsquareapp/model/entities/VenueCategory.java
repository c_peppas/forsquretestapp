package com.example.forsquareapp.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 *
 */
public class VenueCategory {
    public String id;
    @SerializedName("shortName")
    public String name;
    public Icon icon;
}
