package com.example.forsquareapp.model.repo;

import com.example.forsquareapp.model.entities.Venue;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 *
 */
public class VenuesRepositoryImpl implements VenuesRepository {

    private final InMemoryVenuesDataSource memoryDataSource;
    private final RemoteVenuesDataSource remoteDataSource;

    @Inject
    public VenuesRepositoryImpl(InMemoryVenuesDataSource source, RemoteVenuesDataSource source2) {
        this.memoryDataSource = source;
        this.remoteDataSource = source2;
    }

    @Override public Single<List<Venue>> getRecommendedVenuesByPlace(final String place) {
        Single<Data<List<Venue>>> data = Observable.concat(
                memoryDataSource.getRecommendedVenuesByPlace(place),
                remoteDataSource.getRecommendedVenuesByPlace(place)
                        .doOnNext(listData -> saveVenues(place, listData.getItems()))
        ).filter(data1 -> data1 != null && data1.isUpToDate()).firstOrError();

        return data.map(Data::getItems);
    }

    @Override public void saveVenues(String place, List<Venue> venues) {
        memoryDataSource.saveVenues(place, venues);
    }
}
