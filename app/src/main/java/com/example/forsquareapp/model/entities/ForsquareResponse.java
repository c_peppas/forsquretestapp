package com.example.forsquareapp.model.entities;

import java.util.List;

/**
 *
 */
public class ForsquareResponse<T> {
    public Meta meta;
    public List<Notifications> notifications;
    public T response;
}
