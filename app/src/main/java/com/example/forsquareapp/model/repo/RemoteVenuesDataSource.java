package com.example.forsquareapp.model.repo;

import com.example.forsquareapp.model.entities.Group;
import com.example.forsquareapp.model.entities.Venue;
import com.example.forsquareapp.model.entities.Venues;
import com.example.forsquareapp.network.ForsquareApi;
import com.google.gson.JsonParseException;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.HttpException;
import retrofit2.Response;

import static com.example.forsquareapp.network.ForsquareApi.ALLOW_PHOTOS;
import static com.example.forsquareapp.network.ForsquareApi.AUTH_TOKEN;
import static com.example.forsquareapp.network.ForsquareApi.ITEM_LIMIT;
import static com.example.forsquareapp.network.ForsquareApi.VERSION;


/**
 *
 */
public class RemoteVenuesDataSource implements VenuesDataSource {

    private final ForsquareApi api;

    @Inject
    public RemoteVenuesDataSource(ForsquareApi api) {
        this.api = api;
    }

    @Override public Observable<Data<List<Venue>>> getRecommendedVenuesByPlace(String place) {
        return fetchRecommendedVenues(place)
                .toList()
                .toObservable()
                .map(Data::new);
    }

    @Override public void saveVenues(String place, List<Venue> venues) {
        //void
    }

    private boolean processRecommendedVenuesResponse(Response<Venues> response) throws JsonParseException, HttpException {
        if (response.isSuccessful()) {
            if (response.body() != null && response.body().groups != null) {
                return true;
            } else {
                throw new JsonParseException("Something went wrong with Groups Json Response");
            }
        } else {
            throw new HttpException(response);
        }
    }

    private Observable<Venue> fetchRecommendedVenues(String place) {
        return api.exploreVenues(AUTH_TOKEN, VERSION, place, ALLOW_PHOTOS, ITEM_LIMIT)
                .filter(this::processRecommendedVenuesResponse)
                .flatMapIterable(response -> response.body().groups)
                .filter(group -> Group.TYPE_RECOMMENDED.equals(group.type))
                .flatMapIterable(group -> group.items)
                .map(groupContent -> groupContent.venue);
    }
}
