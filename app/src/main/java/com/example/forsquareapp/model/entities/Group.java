package com.example.forsquareapp.model.entities;

import java.util.List;

/**
 *
 */

public class Group {
    public static final String TYPE_RECOMMENDED = "Recommended Places";

    public String name;
    public String type;
    public List<GroupContent> items;
}
