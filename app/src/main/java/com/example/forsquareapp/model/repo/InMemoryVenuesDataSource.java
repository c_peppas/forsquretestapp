package com.example.forsquareapp.model.repo;

import android.util.ArrayMap;

import com.example.forsquareapp.model.entities.Venue;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 *
 */
public class InMemoryVenuesDataSource implements VenuesDataSource {

    private final ArrayMap<String, Data<List<Venue>>> cache;

    @Inject
    public InMemoryVenuesDataSource(ArrayMap<String, Data<List<Venue>>> cache) {
        this.cache = cache;
    }

    @Override public Observable<Data<List<Venue>>> getRecommendedVenuesByPlace(String place) {
        return Observable.fromCallable(() -> cache.get(place));
    }

    @Override public void saveVenues(String place, List<Venue> venues) {
        cache.put(place, new Data<>(new ArrayList<>(venues)));
    }
}
