package com.example.forsquareapp.network;

import com.example.forsquareapp.model.entities.ForsquareResponse;
import com.example.forsquareapp.utils.Types;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 *
 */
public class ForsquareResponseConverterFactory extends Converter.Factory {
    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations,
                                                            Retrofit retrofit) {
        Type envelopeType = Types.newParameterizedType(ForsquareResponse.class, type);

        Converter<ResponseBody, ForsquareResponse> delegate = retrofit.nextResponseBodyConverter(this, envelopeType, annotations);

        return new ForsquareResponseConverter(delegate);
    }
}
