package com.example.forsquareapp.network;

import com.example.forsquareapp.model.entities.Venues;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ForsquareApi {

    public final static String VERSION = "20170421";
    public final static String AUTH_TOKEN = "J1UOLHHPYVDQ4IN32ZLT55G5VFUU4T1NHC3132EUCDE5KY4U";
    public final static int ALLOW_PHOTOS = 1;
    public final static int ITEM_LIMIT = 30;

    @GET("venues/explore")
    Observable<Response<Venues>> exploreVenues(@Query("oauth_token") String authToken,
                                               @Query("v") String version,
                                               @Query("near") String near,
                                               @Query("venuePhotos") int allowPhotos,
                                               @Query("limit") int limit);

}
