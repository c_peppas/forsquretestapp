package com.example.forsquareapp.network;

/**
 * Encapsulates the api host configuration.
 */
public class EnvironmentConfig {

    private final String scheme;
    private final String baseUrl;
    private final String version;

    public EnvironmentConfig(String scheme, String baseUrl, String version) {
        this.scheme = scheme;
        this.baseUrl = baseUrl;
        this.version = version;
    }

    public String getScheme() {
        return scheme;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getVersion() {
        return version;
    }
}
