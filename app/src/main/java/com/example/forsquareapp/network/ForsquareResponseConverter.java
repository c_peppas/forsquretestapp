package com.example.forsquareapp.network;

import com.example.forsquareapp.model.entities.ForsquareResponse;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 *
 */
public class ForsquareResponseConverter<T> implements Converter<ResponseBody, T> {

    final Converter<ResponseBody, ForsquareResponse<T>> delegate;

    public ForsquareResponseConverter(Converter<ResponseBody, ForsquareResponse<T>> delegate) {
        this.delegate = delegate;
    }

    @Override public T convert(ResponseBody responseBody) throws IOException {
        ForsquareResponse<T> envelope = delegate.convert(responseBody);
        return envelope.response;
    }
}
