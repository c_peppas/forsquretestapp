package com.example.forsquareapp.base;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.inputmethod.InputMethodManager;

import com.example.forsquareapp.di.component.ApplicationComponent;

/**
 * Abstract activity that contains only common things that can be shared across activities.
 * i.e. Provides the main application component to its subclasses.
 */
public class BaseActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    protected ApplicationComponent appComponent() {
        return ((BaseApplication) getApplication()).component();
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
}
