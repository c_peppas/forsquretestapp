package com.example.forsquareapp.base;

import android.app.Application;
import android.content.Context;

import com.example.forsquareapp.BuildConfig;
import com.example.forsquareapp.di.component.ApplicationComponent;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import timber.log.Timber;

public abstract class BaseApplication extends Application {
    private ApplicationComponent mApplicationComponent;

    private RefWatcher refWatcher;
    protected abstract ApplicationComponent createApplicationComponent();

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        refWatcher = LeakCanary.install(this);

        mApplicationComponent = createApplicationComponent();

        initTimber();
        initFresco();
    }

    public ApplicationComponent component() {
        return mApplicationComponent;
    }

    protected void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            //Timber.plant(new CrashReportingTree());
        }
    }

    protected void initFresco() {
        Fresco.initialize(this, mApplicationComponent.frescoConfig());
    }

    public static RefWatcher getRefWatcher(Context context) {
        BaseApplication application = (BaseApplication) context.getApplicationContext();
        return application.refWatcher;
    }
}
