package com.example.forsquareapp.di.component;

import com.example.forsquareapp.di.module.AndroidModule;
import com.example.forsquareapp.di.module.DataModule;
import com.example.forsquareapp.di.module.NetworkModule;
import com.example.forsquareapp.di.module.RxModule;
import com.example.forsquareapp.explore.di.ExploreVenuesComponent;
import com.example.forsquareapp.explore.di.ExploreVenuesModule;
import com.facebook.imagepipeline.core.ImagePipelineConfig;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AndroidModule.class, RxModule.class, NetworkModule.class, DataModule.class})
public interface ApplicationComponent {
    ImagePipelineConfig frescoConfig();
    ExploreVenuesComponent add(ExploreVenuesModule module);
}
