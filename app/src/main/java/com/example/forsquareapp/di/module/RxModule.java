package com.example.forsquareapp.di.module;

import com.example.forsquareapp.utils.schedulers.RxSchedulers;
import com.example.forsquareapp.utils.schedulers.RxSchedulersImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class RxModule {

    @Provides @Singleton
    public RxSchedulers provideSchedulers() {
        return new RxSchedulersImpl();
    }

    @Provides
    public CompositeDisposable provideCompositeDesposable() {
        return new CompositeDisposable();
    }
}
