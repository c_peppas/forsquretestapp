package com.example.forsquareapp.di.module;

import android.app.Application;
import android.content.res.Resources;

import com.example.forsquareapp.network.EnvironmentConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AndroidModule {

    private Application mApplication;

    public AndroidModule(Application application) {
        this.mApplication = application;
    }

    @Provides @Singleton
    public Application provideApplication() {
        return mApplication;
    }

    @Provides @Singleton
    public Resources provideResources() {
        return mApplication.getResources();
    }

    @Provides @Singleton
    public EnvironmentConfig provideEnvironmentConfig() {
        return new EnvironmentConfig("https", "api.foursquare.com", "v2");
    }
}
