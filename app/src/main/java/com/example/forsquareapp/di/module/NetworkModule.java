package com.example.forsquareapp.di.module;

import android.app.Application;

import com.example.forsquareapp.BuildConfig;
import com.example.forsquareapp.network.EnvironmentConfig;
import com.example.forsquareapp.network.ForsquareApi;
import com.example.forsquareapp.network.ForsquareResponseConverterFactory;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static com.jakewharton.byteunits.DecimalByteUnit.MEGABYTES;

@Module
public class NetworkModule {

    @Provides @Singleton
    public OkHttpClient.Builder provideOkHttpClientBuilder() {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS);
    }

    @Provides @Singleton
    public HttpLoggingInterceptor provideLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG
                ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);
    }

    @Provides @Singleton
    public Cache provideCache(Application application) {
        Cache cache = null;
        try {
            cache = new Cache(new File(application.getCacheDir(), "http-cache"), MEGABYTES.toBytes(10));
        } catch (Exception e) {
            Timber.e(e, "Could not create Cache!");
        }
        return cache;
    }

    @Provides @Singleton
    public OkHttpClient provideOkHttpClient(OkHttpClient.Builder builder,
                                            HttpLoggingInterceptor loggingInterceptor,
                                            Cache httpResponseCache

    ) {
        return builder
                .addInterceptor(loggingInterceptor)
                .cache(httpResponseCache)
                .build();
    }

    @Provides @Singleton
    public HttpUrl provideHttpUrl(EnvironmentConfig config) {
        return new HttpUrl.Builder()
                .addEncodedPathSegments(config.getVersion())
                .addPathSegment("")
                .scheme(config.getScheme())
                .host(config.getBaseUrl())
                .build();
    }

    @Provides @Singleton
    public Gson provideGson() {
        return new GsonBuilder().setLenient().create();
    }

    @Provides @Singleton
    public Retrofit provideRetrofit(OkHttpClient client, HttpUrl httpUrl, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(client)
                .addConverterFactory(new ForsquareResponseConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides @Singleton
    public ImagePipelineConfig providesFrescoConfig(Application context, OkHttpClient client) {
        return OkHttpImagePipelineConfigFactory
                .newBuilder(context, client)
                .build();
    }

    @Provides @Singleton
    public ForsquareApi provideForsquareApi(Retrofit api) {
        return api.create(ForsquareApi.class);
    }
}
