package com.example.forsquareapp.di.module;

import android.util.ArrayMap;

import com.example.forsquareapp.model.entities.Venue;
import com.example.forsquareapp.model.repo.Data;
import com.example.forsquareapp.model.repo.InMemoryVenuesDataSource;
import com.example.forsquareapp.model.repo.RemoteVenuesDataSource;
import com.example.forsquareapp.model.repo.VenuesDataSource;
import com.example.forsquareapp.model.repo.VenuesRepository;
import com.example.forsquareapp.model.repo.VenuesRepositoryImpl;
import com.example.forsquareapp.network.ForsquareApi;

import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides @Singleton
    public VenuesRepository provideVenuesRepository(InMemoryVenuesDataSource source, RemoteVenuesDataSource source2) {
        return new VenuesRepositoryImpl(source, source2);
    }

    @Provides @Singleton
    public ArrayMap<String, Data<List<Venue>>> provideVenueDataCache() {
        return new ArrayMap<>();
    }

    @Provides @Singleton
    public VenuesDataSource provideVenuesInMemoryDataSource(ArrayMap<String, Data<List<Venue>>> cache) {
        return new InMemoryVenuesDataSource(cache);
    }

    @Provides @Singleton
    public VenuesDataSource provideVenuesRemoteDataSource(ForsquareApi api) {
        return new RemoteVenuesDataSource(api);
    }
}
