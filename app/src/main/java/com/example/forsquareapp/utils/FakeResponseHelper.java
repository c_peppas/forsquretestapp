package com.example.forsquareapp.utils;

import com.google.gson.Gson;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 *
 */

public final class FakeResponseHelper<T> {

    private final Gson gson;

    public FakeResponseHelper(Gson gson) {
        this.gson = gson;
    }

    public Observable<Response<T>> successResponse(Class classz, String filePath) {
        String source = FileUtils.loadFileFromResource(filePath);
        T object = (T) gson.fromJson(source, classz);
        Response<T> successResponse = Response.success(object);

        return Observable.fromCallable(() -> successResponse);
    }

    public Observable<Response<T>> errorResponse(int code, String filePath) {

        String body = Strings.isBlank(filePath) ? "" : FileUtils.loadFileFromResource(filePath);

        Response<T> errorResponse = Response.error(code, ResponseBody.create(MediaType.parse("UTF-8"), body));

        return Observable.fromCallable(() -> errorResponse);
    }
}
