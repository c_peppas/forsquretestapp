package com.example.forsquareapp.utils.error;

import android.content.Context;
import android.support.annotation.StringRes;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 */

public class MessageBundle {

    @StringRes
    private final int stringResId;
    private Object[] args;

    public MessageBundle(@StringRes int stringResId, Object... formatArgs) {
        this.stringResId = stringResId;
        this.args = formatArgs;
    }

    public MessageBundle(@StringRes int stringResId) {
        this.stringResId = stringResId;
    }

    public String getString(Context context) {
        if (args != null) {
            return context.getString(stringResId, args);
        } else {
            return context.getString(stringResId);
        }
    }

    public int id() {
        return stringResId;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageBundle that = (MessageBundle) o;
        return stringResId == that.stringResId &&
                Arrays.equals(args, that.args);
    }

    @Override public int hashCode() {
        return Objects.hash(stringResId, args);
    }
}
