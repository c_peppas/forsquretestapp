package com.example.forsquareapp.utils;

import android.support.v7.widget.SearchView;

/**
 *
 */
public class OnQueryTextListenerAdapter implements SearchView.OnQueryTextListener {
    @Override public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override public boolean onQueryTextChange(String newText) {
        return false;
    }
}
