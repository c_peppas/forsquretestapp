package com.example.forsquareapp.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

import timber.log.Timber;

/**
 *
 */
public class FileUtils {

    private FileUtils() {
        throw new AssertionError("No instances allowed.");
    }

    public static String loadFileFromResource(String filename) {
        InputStream is = ClassLoader.getSystemResourceAsStream(filename);
        return convertInputStreamToString(is);
    }

    public static String loadFileFromAsset(Context context, String filename) {
        InputStream is = null;
        try {
            is = context.getAssets().open(filename);
        } catch (IOException e) {
            Timber.w(e, "loadJSONFromAsset()");
            return null;
        }
        return convertInputStreamToString(is);
    }

    public static String convertInputStreamToString(InputStream inputStream) {
        String result;
        try {
            int size = inputStream.available();
            byte[] buffer = new byte[size];

            inputStream.read(buffer);
            inputStream.close();

            result = new String(buffer, "UTF-8");
        } catch (IOException e) {
            Timber.w(e, "convertInputStreamToString()");
            return null;
        }
        return result;
    }
}
