package com.example.forsquareapp.utils.error;

import com.example.forsquareapp.R;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.HttpException;

/**
 *
 */
public class ErrorMessageHelperImpl implements MessageHelper {

    @Override public MessageBundle errorMessage(Throwable throwable) {
        if (throwable instanceof IOException) {
            if (throwable instanceof UnknownHostException) {
                return new MessageBundle(R.string.error_no_connection);
            } else if (throwable instanceof SocketTimeoutException) {
                return new MessageBundle(R.string.error_time_out);
            } else {
                return new MessageBundle(R.string.error_unexpected);
            }
        } else if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            return new MessageBundle(R.string.error_http_code, exception.code());
        } else if (throwable instanceof JsonParseException) {
            return new MessageBundle(R.string.error_parsing_error);
        } else {
            return new MessageBundle(R.string.error_unexpected);
        }
    }
}
