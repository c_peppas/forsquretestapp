package com.example.forsquareapp.utils.schedulers;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * TestRxSchedulers implementation that gets injected during unit/integration tests in order to allow
 * immediate synchronization.
 */
public class TestRxSchedulers implements RxSchedulers {
    @NonNull @Override public Scheduler computation() {
        return Schedulers.trampoline();
    }

    @NonNull @Override public Scheduler io() {
        return Schedulers.trampoline();
    }

    @NonNull @Override public Scheduler ui() {
        return Schedulers.trampoline();
    }
}
