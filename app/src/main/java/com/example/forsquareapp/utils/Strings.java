package com.example.forsquareapp.utils;

/**
 * Help class that gives you string related helper methods to be used in classes
 * like Presenters where unit-tested in JVM and they need to be android-object free.
 */
public final class Strings {

    public static final String EMPTY_STRING = "";

    private Strings() {
        throw new AssertionError("No instances allowed.");
    }

    public static boolean isBlank(CharSequence string) {
        return (string == null || string.toString().trim().length() == 0);
    }
}
