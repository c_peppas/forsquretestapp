package com.example.forsquareapp.utils.error;

/**
 *
 */

public interface MessageHelper {
    MessageBundle errorMessage(Throwable throwable);
}
