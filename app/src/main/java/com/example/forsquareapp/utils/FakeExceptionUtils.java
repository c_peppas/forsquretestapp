package com.example.forsquareapp.utils;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 *
 */
public final class FakeExceptionUtils {

    private FakeExceptionUtils() {
        throw new AssertionError("No instances allowed.");
    }

    public static Exception createHttpException(int code) {
        return new HttpException(Response.error(code, ResponseBody.create(MediaType.parse("UTF-8"), "")));
    }
}
