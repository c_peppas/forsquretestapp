package com.example.forsquareapp.utils.schedulers;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * This is the standard impl of a RxSchedulersImpl returning the appropriate Schedulers
 * for production code.
 */
public class RxSchedulersImpl implements RxSchedulers {

    @NonNull @Override public Scheduler computation() {
        return Schedulers.computation();
    }

    @NonNull @Override public Scheduler io() {
        return Schedulers.io();
    }

    @NonNull @Override public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}
